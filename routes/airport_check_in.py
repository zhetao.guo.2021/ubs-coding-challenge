from flask import jsonify, request, Response
import json
import logging
from routes import app

logger = logging.getLogger(__name__)


def merge_insertion_sort(passengers):
    def merge(left, right):
        result = []
        i = j = 0

        while i < len(left) and j < len(right):
            if left[i].askTimeToDeparture() < right[j].askTimeToDeparture():
                result.append(left[i])
                i += 1
            else:
                result.append(right[j])
                j += 1

        result.extend(left[i:])
        result.extend(right[j:])
        return result

    def merge_sort(arr):
        if len(arr) <= 1:
            return arr

        mid = len(arr) // 2
        left = arr[:mid]
        right = arr[mid:]

        left = merge_sort(left)
        right = merge_sort(right)

        return merge(left, right)

    return merge_sort(passengers)

class Passenger:
    def __init__(self, departureTime):
        self.departureTime = departureTime
        self.numberOfRequests = 0
    def askTimeToDeparture(self):
        self.numberOfRequests += 1
        return self.departureTime
    def getNumberOfRequests(self):
        return self.numberOfRequests
    def compare_passengers(p1, p2):
        if p1.askTimeToDeparture() < p2.askTimeToDeparture():
            return -1
        elif p1.askTimeToDeparture() > p2.askTimeToDeparture():
            return 1
        else:
            return 0


    def execute(prioritisation_function, passenger_data, cut_off_time):
        totalNumberOfRequests = 0
        passengers = []
        # Initialise list of passenger instances
        for i in range(len(passenger_data)):
            passengers.append(Passenger(passenger_data[i]))
        # Apply solution and re-shuffle with departure cut-off time
        prioritised_and_filtered_passengers = prioritisation_function(passengers, cut_off_time)

        # Sum totalNumberOfRequests across all passengers
        for i in range(len(passengers)):
            totalNumberOfRequests += passengers[i].getNumberOfRequests()

        # Print sequence of sorted departure times
        prioritised_filtered_list = []
        for i in range(len(prioritised_and_filtered_passengers)):
            print(prioritised_and_filtered_passengers[i].departureTime, end=" ")
            prioritised_filtered_list.append(prioritised_and_filtered_passengers[i].departureTime)

        return {
            "sortedDepartureTimes": prioritised_filtered_list,
            "numberOfRequests": totalNumberOfRequests,
        }


    def prioritisation_function(passengers, cut_off_time):
        prioritised_passengers = []
        #sort then filter the passengers by the cut off time
        sorted_passengers = merge_insertion_sort(passengers)

        for i in range(len(sorted_passengers)):
            if passengers[i].askTimeToDeparture() > cut_off_time:
                prioritised_passengers.append(passengers[i])

        return prioritised_passengers


@app.route("/airport", methods=['POST'])
def airport_check_in():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    result = []
    for record in data:
        res = Passenger.execute(Passenger.prioritisation_function, record["departureTimes"], record["cutOffTime"])
        result.append({"id": record["id"], **res})
    logging.info("My result :{}".format(result))
    return Response(json.dumps(result), mimetype="application/json")