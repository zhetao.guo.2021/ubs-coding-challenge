from flask import jsonify, request
import json
import logging
from routes import app

logger = logging.getLogger(__name__)

@app.route("/greedymonkey", methods=['POST'])
def greedy_monkey():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    res = find_greedy_monkey(data["w"], data["v"], data["f"])
    logging.info("My result :{}".format(res))
    return json.dumps(res)
def find_greedy_monkey(w,v,f):
    n = len(f)
    memo = {}

    def helper(i, remaining_w, remaining_v):
        if i == n:
            return 0

        if (i, remaining_w, remaining_v) in memo:
            return memo[(i, remaining_w, remaining_v)]

        max_val = helper(i + 1, remaining_w, remaining_v)

        if f[i][0] <= remaining_w and f[i][1] <= remaining_v:
            value_with_current = f[i][2] + helper(i + 1, remaining_w - f[i][0], remaining_v - f[i][1])
            max_val = max(max_val, value_with_current)

        memo[(i, remaining_w, remaining_v)] = max_val
        return max_val

    return helper(0, w, v)

