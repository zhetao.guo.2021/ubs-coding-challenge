from flask import jsonify, request
import json
import logging
from routes import app

logger = logging.getLogger(__name__)

@app.route("/lazy-developer", methods=['POST'])
def lazy_developer():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    res = getNextProbableWords(data["classes"], data["statements"])
    return json.dumps(res)

def match(s, c):
    return c.startswith(s) and len(c.split(".")) == len(s.split("."))

def getNextProbableWords(classes,statements):
    c_1d = []

    valid_types = [k for c in classes for k in c]

    def get_flatten_from_list(l):
        r = []
        for v in l:
            r.append(k + "." + v)
            if v in valid_types:
                types = [y for x in classes for y in x.get(v, [])]
                for t in types:
                    r.append(k + "." + v + "." + t)
        return r

    def get_flatten_from_dict(d):
        r = []
        for v in d:
            r.append(k + "." + v)
            if d[v] in valid_types:
                types = [x.get(d[v]) for x in classes if x.get(d[v])][0]
                for t in types:
                    r.append(k + "." + v + "." + t)
        return r

    for c in classes:
        for k in c:
            if isinstance(c[k], list):
                c_1d.extend(get_flatten_from_list(c[k]))
            elif isinstance(c[k], dict):
                c_1d.extend(get_flatten_from_dict(c[k]))

    out = dict()

    for s in statements:
        most_probable = list(filter(lambda c: match(s, c), c_1d))
        possible = sorted([m.split(".")[-1] for m in most_probable])
        if len(possible) == 0:
            possible = [""]
        out[s] = possible

    return out