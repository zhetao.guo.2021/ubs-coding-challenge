import logging
import json
from routes import app
from flask import jsonify, request, Response

@app.route("/pie-chart", methods=['POST'])
def portfolio():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    res = calculate_angles(data)
    logging.info("My result :{}".format(res))
    return Response(json.dumps(res) , mimetype='application/json')

def calculate_angles(data):
    portfolio = data['data']
    total_investment = sum([instrument['quantity'] * instrument['price'] for instrument in portfolio])
    instruments = sorted(portfolio, key=lambda x: x['quantity'] * x['price'], reverse=True)
    min_radians = 0.00314159
    total_angle = 2 * 3.141592653589793
    angles = [round(0.0, 8)]

    instrument_angles = []
    excess = 0.0
    for instrument in instruments:
        value = instrument['quantity'] * instrument['price']
        angle = total_angle * (value / total_investment)
        if angle < min_radians:
            excess += min_radians - angle
            angle = min_radians
        instrument_angles.append(angle)

    num_adjusted = sum(1 for angle in instrument_angles if angle != min_radians)
    for idx, angle in enumerate(instrument_angles):
        if angle != min_radians:
            instrument_angles[idx] -= excess / num_adjusted
    cum_angle = 0.0
    for angle in instrument_angles:
        cum_angle += angle
        angles.append(round(cum_angle, 8))

    return {
        "instruments": angles
    }