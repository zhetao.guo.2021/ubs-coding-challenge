import json
import logging
import math
import re
from routes import app

from flask import jsonify, request, Response

logger = logging.getLogger(__name__)

@app.route("/swissbyte", methods=['POST'])
def swiss_byte():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    res = run_program(data["code"], data["cases"])
    logging.info("My result :{}".format(res))
    return Response(json.dumps(res), mimetype='application/json')


TOKENS = [
    ('WHITESPACE', r'\s+'),
    ('FAIL', r'fail'),
    ('IF', r'if'),
    ('ENDIF', r'endif'),
    ('NAME', r'[a-zA-Z]\w*'),
    ('ASSIGN', r'='),
    ('OP', r'[+\-*/]'),
    ('CMP', r'==|!=|<|>|<=|>='),
    ('INTEGER', r'\d+'),
]

def lexer(code):
    tokens = []
    while code:
        for token, pattern in TOKENS:
            match = re.match(pattern, code)
            if match:
                value = match.group(0)
                if token != 'WHITESPACE':
                    tokens.append((token, value))
                code = code[len(value):]
                break
    return tokens

class Expr:
    pass

class If(Expr):
    def __init__(self, cond, stmts):
        self.cond = cond
        self.stmts = stmts

class Operate(Expr):
    def __init__(self, name, val1, op, val2):
        self.name = name
        self.val1 = val1
        self.op = op
        self.val2 = val2

class Assign(Expr):
    def __init__(self, name, val):
        self.name = name
        self.val = val

class Fail(Expr):
    pass

def parse(tokens):
    if not tokens:
        return []

    stmts = []

    while tokens:
        token, value = tokens.pop(0)
        if token == "IF":
            cond = (tokens.pop(0), tokens.pop(0), tokens.pop(0))
            print(f"{cond[0][1]} {cond[1][1]} {cond[2][1]}")
            inner_stmts = parse(tokens)
            _, _ = tokens.pop(0)
            stmts.append(If(cond, inner_stmts))
        elif token == "NAME":
            if tokens[0][0] == "ASSIGN":
                _, _ = tokens.pop(0)
                if tokens[0][0] == "INTEGER" or (tokens[0][0] == "NAME" and tokens[1][0] != "OP"):
                    _, val = tokens.pop(0)
                    stmts.append(Assign(value, val))
                else:
                    val1, _ = tokens.pop(0)
                    op, _ = tokens.pop(0)
                    val2, _ = tokens.pop(0)
                    stmts.append(Operate(value, val1, op, val2))
        elif token == "ENDIF":
            return stmts
        elif token == "FAIL":
            stmts.append(Fail())
    return stmts


def evaluate(exprs, case):
    for expr in exprs:
        if isinstance(expr, If):
            name1, cmp, name2 = expr.cond
            val1 = case.get(name1[1], int(name1[1])) if name1[0] == "NAME" else int(name1[1])
            val2 = case.get(name2[1], int(name2[1])) if name2[0] == "NAME" else int(name2[1])
            condition_met = False
            if cmp[1] == "==" and val1 == val2:
                condition_met = True
            elif cmp[1] == "!=" and val1 != val2:
                condition_met = True
            elif cmp[1] == "<" and val1 < val2:
                condition_met = True
            elif cmp[1] == ">" and val1 > val2:
                condition_met = True


            if condition_met:
                result, case = evaluate(expr.stmts, case)
                if not result:
                    return False, case
            continue
        elif isinstance(expr, Operate):
            print(f"Operation: {expr.name} {expr.val1} {expr.op} {expr.val2}")

            if expr.val1.isnumeric():
                val1 = int(expr.val1)
            else:
                val1 = case.get(expr.val1, None)
                if val1 is None:
                    return False, case

            if expr.val2.isnumeric():
                val2 = int(expr.val2)
            else:
                val2 = case.get(expr.val2, None)
                if val2 is None:
                    return False, case

            if expr.op == "+":
                case[expr.name] = val1 + val2
            elif expr.op == "-":
                case[expr.name] = val1 - val2
            elif expr.op == "*":
                case[expr.name] = val1 * val2
            elif expr.op == "/":
                case[expr.name] = math.floor(val1 / val2)


        elif isinstance(expr, Assign):
            if expr.val.isnumeric():
                val = int(expr.val)
            else:
                val = case.get(expr.val)
                if val is None:
                    return False, case
            case[expr.name] = val
        elif isinstance(expr, Fail):
            return False, case
    return True, case

def run_program(code, cases):
    tokens = lexer("\n".join(code))
    ast = parse(tokens)

    outcomes = []
    for case in cases:
        is_solvable, variables = evaluate(ast, case)
        outcomes.append({
            "is_solvable": is_solvable,
            "variables": variables
        })
    return {"outcomes": outcomes}
