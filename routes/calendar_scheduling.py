from flask import jsonify, request, Response
import json
import logging
from routes import app

logger = logging.getLogger(__name__)

@app.route("/calendar-scheduling", methods=['POST'])
def calendar_scheduling():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    res = schedule_lessons(data)
    logging.info("My result :{}".format(res))
    return Response(json.dumps(res) , mimetype='application/json')

def schedule_lessons(lesson_requests):
    days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
    sorted_lessons = sorted(lesson_requests, key=lambda x: (x["potentialEarnings"]/x["duration"] if x["duration"] != 0 else float('inf'), x["potentialEarnings"]), reverse=True)
    schedule = {day: [] for day in days}
    hours_allocated = {day: 0 for day in days}
    for lesson in sorted_lessons:
        for day in lesson["availableDays"]:
            if hours_allocated[day] + lesson["duration"] <= 12:
                schedule[day].append(lesson["lessonRequestId"])
                hours_allocated[day] += lesson["duration"]
                break
    final_schedule = {day: lessons for day, lessons in schedule.items() if lessons}
    return final_schedule