import json
import logging
import math
from routes import app
from flask import jsonify, request, Response

logger = logging.getLogger(__name__)


@app.route("/teleportation", methods=['POST'])
def teleportation():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    res = best_route(data["k"], data["p"], data["q"])
    logging.info("My result :{}".format(res))
    return json.dumps(res)

def calculate_euclidean_distance(point1, point2):
    return math.dist(point1, point2)

def best_route(k, p, q):
    start = [0,0]
    total_distance = 0

    for delivery in q:
        min_distance = calculate_euclidean_distance(start,delivery)
        if k > 0:
            for teleport in p:
                teleport_to_delivery_distance = calculate_euclidean_distance(teleport, delivery)
                if teleport_to_delivery_distance < min_distance:
                    min_distance = teleport_to_delivery_distance
                    k -= 1
        total_distance += min_distance
        start = delivery
    return round(total_distance,2)

