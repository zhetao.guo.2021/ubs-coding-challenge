from flask import jsonify, request
import json
import logging
from routes import app

logger = logging.getLogger(__name__)

@app.route("/maze", methods=['POST'])
def maze():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    next_move = decide_next_move(data)
    return json.dumps(next_move)

def decide_next_move(data):
    nearby = data["nearBy"]
    isPreviousMovementValid = data["isPreviousMovementValid"]

    # Define order of preference for directions
    directions = ["up", "right", "down", "left"]
    nearby_values = [
        nearby[0][1],  # Up
        nearby[1][2],  # Right
        nearby[2][1],  # Down
        nearby[1][0]  # Left
    ]
    for direction, value in zip(directions, nearby_values):
        if value == 3:
            return direction

    for direction, value in zip(directions, nearby_values):
        if value == 1:
            return direction
    if not isPreviousMovementValid:
        return "respawn"
    return "respawn"