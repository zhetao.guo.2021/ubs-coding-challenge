from flask import Flask

app = Flask(__name__)
import routes.square
import routes.lazy_developer
import routes.greedy_monkey
import routes.railway_builder
import routes.airport_check_in
import routes.calendar_scheduling
import routes.teleportation
import routes.portfolio
import routes.swissbyte